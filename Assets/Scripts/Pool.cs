﻿using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class Pool<T> where T: Behaviour, IPoolable<T>
{
    private const string POOL_ROOT = "[Pools]";

    private Transform activeRoot;
    private Transform root;

    private List<T> objects;

    private readonly Transform passiveRoot;
    private readonly T prefab;

    public Pool(Transform passiveRoot, T prefab, int size)
    {
        objects = new List<T>();
        this.passiveRoot = passiveRoot;
        this.prefab = prefab;

        CreateActiveRoot();
        Generate(size);
    }

    public T GetObject()
    {
        T obj = default(T);

        for (var i = 0; i < objects.Count; i++)
        {
            if (!objects[i].gameObject.activeSelf)
            {
                obj = objects[i];
                break;
            }
        }

        if (obj == null)
        {
            obj = CreateObject();
        }

        obj.OnPooledBackEvent += GiveBack;
        obj.transform.SetParent(activeRoot, false);
        obj.transform.localScale = Vector3.one;

        return obj;
    }

    public void GiveBack(T obj)
    {
        if (obj != null)
        {
            obj.gameObject.SetActive(false);
            obj.transform.SetParent(passiveRoot, false);
        }
        else
        {
            Debug.LogWarning("You try give back GameObject that is null!");
        }
    }

    private void CreateActiveRoot()
    {
        GameObject gameObjectroot = GameObject.Find(POOL_ROOT);

        if (gameObjectroot == null)
        {
            root = new GameObject(POOL_ROOT).transform;
        }
        else
        {
            root = gameObjectroot.transform;
        }

        if (activeRoot == null)
        {
            CreateLocalRoot();
        }
    }

    private void CreateLocalRoot()
    {
        activeRoot = new GameObject(string.Format("[{0}]", prefab.name)).transform;
        activeRoot.transform.SetParent(root);
    }

    private void Generate(int size)
    {
        for (var i = 0; i < size; i++)
        {
            var obj = CreateObject();
            obj.gameObject.SetActive(false);
        }
    }

    private T CreateObject()
    {
        var obj = Object.Instantiate(prefab);
        obj.gameObject.RemoveClonedName();
        obj.name = string.Format("{0}_{1}", obj.name, objects.Count);
        obj.transform.SetParent(passiveRoot, false);
        objects.Add(obj);

        return obj;
    }
}