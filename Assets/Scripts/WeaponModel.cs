﻿using UnityEngine;

[CreateAssetMenu(fileName = "WeaponModel", menuName = "WeaponModel", order = 1)]
public class WeaponModel : ScriptableObject
{
    [SerializeField] private CollisionDamagerModel collisionDamagerModel;

    public CollisionDamagerModel CollisionDamagerModel
    {
        get { return collisionDamagerModel; }
    }
}