﻿using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    public void Move(BaseTrajectory trajectory, Vector3 start, Vector3 end)
    {
        trajectory.Initialize(transform, start, end);
    }
}