﻿using System;
using UnityEngine;

[Serializable]
public class StatModel: IModel
{
    public event Action OnChangedEvent;
    public event Action OnReachedZeroEvent;

    [SerializeField] private StatParameterModel currentValue = new StatParameterModel();
    [SerializeField] private StatParameterModel maximumValue = new StatParameterModel();

    public StatParameterModel CurrentValue
    {
        get { return currentValue; }
        set { currentValue = value; }
    }

    public StatParameterModel MaximumValue
    {
        get { return maximumValue; }
        set { maximumValue = value; }
    }

    public void Initialize()
    {
        CurrentValue.OnChangedEvent += OnChanged;
        MaximumValue.OnChangedEvent += OnChanged;
    }

    ~StatModel()
    {
        CurrentValue.OnChangedEvent -= OnChanged;
        MaximumValue.OnChangedEvent -= OnChanged;
    }

    private void OnChanged()
    {
        OnChangedEvent?.Invoke();

        if (currentValue.Value <= 0)
        {
            OnReachedZeroEvent?.Invoke();
        }
    }
}