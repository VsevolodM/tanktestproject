﻿using System;
using UnityEngine;

[Serializable]
public abstract class BaseCharacterModel : ScriptableObject, IModel, IDestroyableModel
{
    public event Action OnKilledEvent;

    [SerializeField] private StatModel health = new StatModel();

    [Range(0,1)]
    [SerializeField] private float protection;
    [SerializeField] private float movementSpeed;

    public bool IsDead()
    {
        return Health.CurrentValue.Value <= 0f;
    }

    public StatModel Health
    {
        get { return health; }
    }

    public float Protection
    {
        get { return protection; }
    }

    public float MovementSpeed
    {
        get { return movementSpeed; }
    }

    private void OnDestroy()
    {
        health.OnReachedZeroEvent -= OnHealthReachedZero;
    }

    private void OnHealthReachedZero()
    {
        OnKilledEvent?.Invoke();
    }

    public virtual void Initialize()
    {
        health.Initialize();
        health.OnReachedZeroEvent += OnHealthReachedZero;
    }
}