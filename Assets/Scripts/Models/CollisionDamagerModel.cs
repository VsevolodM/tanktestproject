﻿using UnityEngine;

[CreateAssetMenu(fileName = "CollisionDamager", menuName = "CollisionDamager", order = 1)]
public class CollisionDamagerModel : ScriptableObject
{
    [SerializeField] private int damage = 10;
    [SerializeField] private bool destroyOnCollision;

    public int Damage { get { return damage; } }
    public bool DestroyOnCollision { get { return destroyOnCollision; } }
}