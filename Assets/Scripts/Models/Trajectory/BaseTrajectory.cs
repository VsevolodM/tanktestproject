﻿using UnityEngine;

public abstract class BaseTrajectory : ScriptableObject
{
    [SerializeField] protected float speed = 5f;
    protected Transform movingObject { get; private set; }
    protected Vector3 start { get; set; }
    protected Vector3 end { get; set; }
    protected Rigidbody rigidbody;

    public virtual void Initialize(Transform transform, Vector3 start, Vector3 end)
    {
        movingObject = transform;
        this.start = start;
        this.end = end;
        OnInitialize();
    }

    protected virtual void OnInitialize()
    {
        rigidbody = movingObject.GetComponent<Rigidbody>();
        Move();
    }

    protected virtual void Move()
    {
        rigidbody.velocity = Vector3.zero;
    }
}