﻿using UnityEngine;

[CreateAssetMenu(fileName = "LineTrajectory", menuName = "LineTrajectory", order = 1)]
public class LineTrajectory : BaseTrajectory
{
    protected override void Move()
    {
        base.Move();
        rigidbody.AddForce(movingObject.transform.forward * speed, ForceMode.Impulse);
    }
}