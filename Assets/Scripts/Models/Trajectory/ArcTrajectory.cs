﻿using UnityEngine;

[CreateAssetMenu(fileName = "ArcTrajectory", menuName = "ArcTrajectory", order = 1)]
public class ArcTrajectory : BaseTrajectory
{
    [SerializeField] private float angle = 70f;

    protected override void Move()
    {
        base.Move();
        start = new Vector3(start.x, 0.0f, start.z);
        end = new Vector3(end.x, 0.0f, end.z);

        movingObject.LookAt(end);

        float distance = Vector3.Distance(start, end);
        float tanAlpha = Mathf.Tan(angle * Mathf.Deg2Rad);
        float height = end.y - start.y;

        float vz = Mathf.Sqrt(Physics.gravity.y * distance * distance / (2.0f * (height - distance * tanAlpha)));
        float vy = tanAlpha * vz;

        Vector3 localVelocity = new Vector3(0f, vy, vz);
        Vector3 globalVelocity = movingObject.TransformDirection(localVelocity);
        rigidbody.velocity = globalVelocity;
    }
}