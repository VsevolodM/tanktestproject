﻿using System;
using UnityEngine;

[Serializable]
public class StatParameterModel
{
    public event Action OnChangedEvent;

    [SerializeField] private int value;

    public int Value
    {
        get => value;
        set => this.value = value;
    }

    public void Add(int value)
    {
        Set(Value + value);
    }

    public void Reduce(int value)
    {
        Set(Value - value);
    }

    public void Set(int value)
    {
        if (value < 0)
        {
            value = 0;
        }

        Value = value;
        OnChangedEvent?.Invoke();
    }
}