﻿using UnityEngine;

[CreateAssetMenu(fileName = "CharacterModel", menuName = "CharacterModel", order = 1)]
public class CharacterModel : BaseCharacterModel
{
    [SerializeField] private BaseAIBehaviour behaviour;
    [SerializeField] private AttitudeTowardsPlayer attitude;

    public BaseAIBehaviour Behaviour
    {
        get { return behaviour; }
    }

    public override void Initialize()
    {
        base.Initialize();
        behaviour = Instantiate(behaviour);
    }
}

public enum AttitudeTowardsPlayer
{
    Neutral,
    Friendly,
    Hostile
}
