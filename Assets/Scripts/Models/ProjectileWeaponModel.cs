﻿using UnityEngine;

[CreateAssetMenu(fileName = "ProjectileWeaponModel", menuName = "ProjectileWeaponModel", order = 1)]
public class ProjectileWeaponModel: WeaponModel
{
    [SerializeField] private float distance;
    [SerializeField] private BaseTrajectory trajectory;

    public BaseTrajectory Trajectory
    {
        get { return trajectory; }
    }

    public float Distance
    {
        get { return distance; }
    }
}