﻿using UnityEngine;

public abstract class BaseAIBehaviour: ScriptableObject
{
    protected PlayerView player;
    protected CharacterView view;
    private Vector3 originalPosition;

    public void Initialize(CharacterView view)
    {
        this.view = view;
        OnInitialize();
    }

    public virtual void OnInitialize()
    {
        originalPosition = view.transform.position;
    }

    public virtual void Start()
    {
        GetPlayerReference();
        UpdateProvider.Instance.OnUpdateEvent -= OnUpdate;
        UpdateProvider.Instance.OnUpdateEvent += OnUpdate;
    }

    public virtual void Stop()
    {
        UpdateProvider.Instance.OnUpdateEvent -= OnUpdate;
    }

    public void ReturnToOriginalPosition()
    {
        view.Agent.SetDestination(originalPosition);
    }

    protected virtual void OnUpdate(float deltaTime)
    {

    }

    protected void GetPlayerReference()
    {
        player = view.FindPlayerView();
    }

    public void Uninitialize()
    {
        Stop();
        player = null;
        view = null;
    }
}