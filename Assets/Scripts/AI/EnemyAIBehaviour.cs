﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyAIBehaviour", menuName = "EnemyAIBehaviour")]
public class EnemyAIBehaviour : BaseAIBehaviour
{
    [SerializeField] private float damageInterval = 0.8f;
    private WaitForSeconds delay;
    private Coroutine damageCoroutine;

    public override void OnInitialize()
    {
        base.OnInitialize();
        delay = new WaitForSeconds(damageInterval);
    }

    protected override void OnUpdate(float deltaTime)
    {
        base.OnUpdate(deltaTime);

        if (player.Model.IsDead())
        {
            Stop();
            ReturnToOriginalPosition();
            return;
        }

        if (damageCoroutine != null)
        {
            ThreadTools.StopCoroutine(DoDamageCoroutine());
        }

        if (IsNearTarget())
        {
            if (view.Agent.remainingDistance <= view.Agent.stoppingDistance && !view.Agent.pathPending)
            {
                if (damageCoroutine == null)
                {
                    damageCoroutine = ThreadTools.StartCoroutine(DoDamageCoroutine());
                }
            }
        }
        else
        {
            Vector3 targetPosition = GetClosesPointToTarget();
            view.Agent.SetDestination(targetPosition);
        }
    }

    private bool IsNearTarget()
    {
        Vector3 targetPosition = GetClosesPointToTarget();
        return Vector3.Distance(view.transform.position, targetPosition) <= 0.2f;
    }

    private Vector3 GetClosesPointToTarget()
    {
        Vector3 closestPoint = player.Collider.bounds.ClosestPoint(view.transform.position);
        closestPoint = new Vector3(closestPoint.x, 0f, closestPoint.z);
        return closestPoint;
    }

    private IEnumerator DoDamageCoroutine()
    {
        while (!player.Model.IsDead() && IsNearTarget())
        {
            int damage = GameData.GetDamage(view.ActiveProjectileWeapon.WeaponModel.CollisionDamagerModel.Damage, 
                player.Model.Protection);

            player.Model.Health.CurrentValue.Reduce(damage);
            yield return delay;
        }
    }
}