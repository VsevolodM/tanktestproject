﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpawner : MonoBehaviour
{
    [SerializeField] private int maximum = 10;
    [SerializeField] private float spawnInterval = 2f;
    [SerializeField] private Transform poolParent;
    [Space(20)]
    [SerializeField] private SpawnSettings[] spawnSettings;
    [Space(20)]
    [SerializeField] private Color areaColor;
    [SerializeField] private Transform[] spawnAreas;

    private List<Pool<BaseCharacterView>> pools = new List<Pool<BaseCharacterView>>();
    
    private Coroutine spawningCoroutine;
    private List<BaseCharacterView> activeCharacters;

    private void Awake()
    {
        activeCharacters = new List<BaseCharacterView>();

        for (int i = 0; i < spawnSettings.Length; i++)
        {
            pools.Add(new Pool<BaseCharacterView>(poolParent, spawnSettings[i].View, 1));
        }

        spawningCoroutine = StartCoroutine(SpawningCoroutine());
    }

    private IEnumerator SpawningCoroutine()
    {
        WaitForSeconds delay = new WaitForSeconds(spawnInterval);

        while (true)
        {
            yield return delay;

            if (activeCharacters.Count < maximum)
            {
                Spawn();
            }
        }
    }

    private void OnDestroy()
    {
        if (spawningCoroutine != null)
        {
            StopCoroutine(spawningCoroutine);
        }
    }

    private void Spawn()
    {
        int randomIndex = Random.Range(0, spawnSettings.Length);

        SpawnSettings settings = spawnSettings[randomIndex];
        Transform randomSpawnArea = spawnAreas.GetRandom();

        Vector3 randomPosition = randomSpawnArea.GetRandomPointInside();

        BaseCharacterView newCharacter = pools[randomIndex].GetObject();
        newCharacter.transform.position = randomPosition;

        BaseCharacterModel model = Instantiate(settings.Model);
        newCharacter.Initialize(model);
        newCharacter.gameObject.SetActive(true);

        newCharacter.OnPooledBackEvent += RemoveCharacter;

        activeCharacters.Add(newCharacter);
    }

    private void RemoveCharacter(BaseCharacterView character)
    {
        activeCharacters.Remove(character);
        character.OnPooledBackEvent -= RemoveCharacter;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = areaColor;

        for (int i = 0; i < spawnAreas.Length; i++)
        {
            if (spawnAreas[i] != null)
            {
                Gizmos.DrawCube(spawnAreas[i].position, spawnAreas[i].lossyScale);
            }
        }
    }
}

[System.Serializable]
struct SpawnSettings
{
    [SerializeField] private BaseCharacterView view;
    [SerializeField] private BaseCharacterModel model;

    public BaseCharacterView View {
        get { return view; }
    }

    public BaseCharacterModel Model
    {
        get { return model; }
    }
}