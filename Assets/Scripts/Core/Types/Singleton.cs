﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    private static readonly object Lock = new object();

    public static T Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                return null;
            }

            lock (Lock)
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        return instance;
                    }

                    if (instance == null)
                    {
                        GameObject singleton = new GameObject(string.Format("{{{0}}}", typeof(T)));
                        instance = singleton.AddComponent<T>();
                        singleton.name = "(singleton) " + typeof(T);

                        GameObject parent = GameObject.Find("[Singletones]");
                        if (parent == null)
                        {
                            parent = new GameObject("[Singletones]");
                        }
                        singleton.transform.SetParent(parent.transform);

                        DontDestroyOnLoad(parent);
                    }
                }

                return instance;
            }
        }
    }

    private static bool applicationIsQuitting;

    public virtual void OnDestroy()
    {
        applicationIsQuitting = true;
    }
}
