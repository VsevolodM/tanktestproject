﻿public interface IDestroyableView
{
    IDestroyableModel GetIDestroyable();
}