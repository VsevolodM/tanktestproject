﻿public interface IView<T>
{
    T Model { get; }
    void Initialize(T model);
}