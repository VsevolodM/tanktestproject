﻿public interface IDestroyableModel
{
    StatModel Health { get; }
}