﻿using System;
using UnityEngine;

public interface IPoolable<T> where T : Behaviour
{
    event Action<T> OnPooledBackEvent;
    void PoolBack();
}