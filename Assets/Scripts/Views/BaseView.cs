﻿using UnityEngine;

public abstract class BaseView<T> : MonoBehaviour, IView<T> where T: IModel
{
    public T Model { get; private set; }

    public void Initialize(T model)
    {
        Model = model;
        model.Initialize();
        OnInitialize();
    }

    protected virtual void OnInitialize()
    {

    }

    private void OnDestroy()
    {
        Uninitialize();
    }

    private void Uninitialize()
    {
        Model = default(T);
        OnUninitialize();
    }

    protected virtual void OnUninitialize()
    {

    }
}