﻿using UnityEngine;

public class ProjectileWeaponView : WeaponView
{
    [SerializeField] private CollisionDamagerView projectile;
    [SerializeField] private Transform projectileSpawnpoint;
    [SerializeField] private Transform poolTransform;

    private Pool<CollisionDamagerView> pool;
    private ProjectileWeaponModel projectileWeaponModel;

    private void Awake()
    {
        pool = new Pool<CollisionDamagerView>(poolTransform, projectile, 5);
        projectileWeaponModel = (ProjectileWeaponModel)WeaponModel;
    }

    public override void Hit()
    {
        Vector3 targetPosition = transform.position + transform.forward * projectileWeaponModel.Distance;

        CollisionDamagerView newProjectile = pool.GetObject();
        ObjectMover objectMover = newProjectile.gameObject.AddOrGetComponent<ObjectMover>();
        newProjectile.transform.position = projectileSpawnpoint.position;
        newProjectile.transform.rotation = projectileSpawnpoint.rotation;
        newProjectile.gameObject.SetActive(true);
        objectMover.Move(projectileWeaponModel.Trajectory, projectileSpawnpoint.position, targetPosition);
    }
}
