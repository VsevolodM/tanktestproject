﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatView : BaseView<StatModel>
{
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI fillText;

    protected override void OnInitialize()
    {
        base.OnInitialize();
        Model.OnChangedEvent += OnChange;
    }

    private void OnChange()
    {
        fillText.text = string.Format("{0}/{1}", Model.CurrentValue.Value, Model.MaximumValue.Value);
        fill.fillAmount = (float)Model.CurrentValue.Value / Model.MaximumValue.Value;
    }

    private void OnDestroy()
    {
        Model.OnChangedEvent -= OnChange;
    }
}
