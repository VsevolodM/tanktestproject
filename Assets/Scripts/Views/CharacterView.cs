﻿using UnityEngine;
using UnityEngine.AI;

[SelectionBase]
public class CharacterView : BaseCharacterView
{
    public NavMeshAgent Agent { get; private set; }
    
    private bool wasInitialized;

    protected override void Awake()
    {
        base.Awake();
        Agent = GetComponent<NavMeshAgent>();
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        
        Agent.speed = Model.MovementSpeed;

        CharacterModel model = (CharacterModel)Model;
        model.Behaviour.Initialize(this);
        model.Behaviour.Start();

        wasInitialized = true; 
    }

    private void OnEnable()
    {
        if (wasInitialized)
        {
            ((CharacterModel)Model).Behaviour.Start();
        }
    }

    private void OnDisable()
    {
        if (wasInitialized)
        {
            ((CharacterModel)Model).Behaviour.Stop();
        }
    }

    private void OnDestroy()
    {
        if (wasInitialized)
        {
            ((CharacterModel)Model).Behaviour.Uninitialize();
        }
    }
}
