﻿using System;
using UnityEngine;

public abstract class BaseCharacterView : BaseView<BaseCharacterModel>, IPoolable<BaseCharacterView>, IDestroyableView
{
    public event Action<BaseCharacterView> OnPooledBackEvent;

    [SerializeField] private WeaponView[] weapon;

    public WeaponView[] Weapon
    {
        get { return weapon; }
        protected set { weapon = value; }
    }

    public WeaponView ActiveProjectileWeapon { get; protected set; }

    protected virtual void Awake()
    {
        ActiveProjectileWeapon = Weapon[0];
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        Model.OnKilledEvent += OnKilled;
    }

    private void OnDestroy()
    {
        Model.OnKilledEvent -= OnKilled;
    }

    private void OnKilled()
    {
        PoolBack();
    }

    public void PoolBack()
    {
        OnPooledBackEvent?.Invoke(this);
    }

    public PlayerView FindPlayerView()
    {
        return FindObjectOfType<PlayerView>();
    }

    public IDestroyableModel GetIDestroyable()
    {
        return Model;
    }
}