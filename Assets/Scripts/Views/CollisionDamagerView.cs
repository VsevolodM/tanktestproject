﻿using System;
using UnityEngine;

public class CollisionDamagerView : MonoBehaviour, IPoolable<CollisionDamagerView>
{
    public event Action<CollisionDamagerView> OnPooledBackEvent;

    [SerializeField] private LayerMask mask;
    [SerializeField] private CollisionDamagerModel model;

    private void OnTriggerEnter(Collider other)
    {
        if (mask == (mask | (1 << other.gameObject.layer)))
        {
            IDestroyableView iDestroyableView = other.GetComponent<IDestroyableView>();

            if(iDestroyableView != null)
            { 
                IDestroyableModel iDestroyableModel = iDestroyableView.GetIDestroyable();

                iDestroyableModel?.Health.CurrentValue.Reduce(model.Damage);

                if (model.DestroyOnCollision)
                {
                    PoolBack();
                }
            }
        }
    }

    public void PoolBack()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();

        if (rigidbody != null)
        {
            rigidbody.velocity = Vector3.zero;
        }

        OnPooledBackEvent?.Invoke(this);
    }
}