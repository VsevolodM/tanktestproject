﻿using UnityEngine;

public class PlayerView : BaseCharacterView
{
    [SerializeField] private PlayerInputController playerInputController;
    [SerializeField] private CameraMouseRotator cameraMouseRotator;
    [SerializeField] private PlayerModel model;
    [SerializeField] private BoxCollider collider;
    [SerializeField] private StatView healthView;

    public BoxCollider Collider
    {
        get { return collider; }
    }

    protected override void Awake()
    {
        base.Awake();
        playerInputController.SetWeaponAmount(Weapon.Length);
        Initialize(Instantiate(model));
    }

    protected override void OnInitialize()
    {
        base.OnInitialize();
        healthView.Initialize(Model.Health);
        cameraMouseRotator.OnCameraRotatedEvent += playerInputController.SetHeadRotation;
        playerInputController.OnWeaponChangedEvent += OnWeaponIndexChanged;
        playerInputController.OnShootPressedEvent += OnShootPressed;
        Model.OnKilledEvent += OnKilled;
    }

    private void OnKilled()
    {
        model.OnKilledEvent -= OnKilled;
        playerInputController.enabled = false;
        cameraMouseRotator.enabled = false;
    }

    protected override void OnUninitialize()
    {
        base.OnUninitialize();
        cameraMouseRotator.OnCameraRotatedEvent -= playerInputController.SetHeadRotation;
        playerInputController.OnWeaponChangedEvent -= OnWeaponIndexChanged;
        playerInputController.OnShootPressedEvent -= OnShootPressed;
    }

    private void OnWeaponIndexChanged(int index)
    {
        ActiveProjectileWeapon = Weapon[index];
    }

    private void OnShootPressed()
    {
        ActiveProjectileWeapon.Hit();
    }
}
