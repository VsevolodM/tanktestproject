﻿using UnityEngine;

public class WeaponView : MonoBehaviour
{
    [SerializeField] private WeaponModel weaponModel;
    public WeaponModel WeaponModel { get { return weaponModel; } }

    public virtual void Hit()
    {

    }
}
