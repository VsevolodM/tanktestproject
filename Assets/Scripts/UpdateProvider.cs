﻿using System;
using UnityEngine;

public class UpdateProvider : Singleton<UpdateProvider>
{
    public event Action<float> OnUpdateEvent;
    public event Action<float> OnFixedUpdateEvent;
    public event Action<float> OnLateUpdateEvent;

    private void Update()
    {
        OnUpdateEvent?.Invoke(Time.deltaTime);
    }

    private void FixedUpdate()
    {
        OnFixedUpdateEvent?.Invoke(Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        OnLateUpdateEvent?.Invoke(Time.deltaTime);
    }
}
