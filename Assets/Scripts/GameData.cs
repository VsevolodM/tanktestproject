﻿using UnityEngine;

public static class GameData 
{
    public static int GetDamage(float damage, float protection)
    {
        return Mathf.RoundToInt(damage * (1 - protection));
    }
}
