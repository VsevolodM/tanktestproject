﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static T AddOrGetComponent<T>(this GameObject gameObject) where T : Behaviour
    {
        T behaviour = gameObject.GetComponent<T>();

        if (behaviour == null)
        {
            behaviour = gameObject.AddComponent<T>();
        }

        return behaviour;
    }

    public static void RemoveClonedName(this GameObject gameObject)
    {
        gameObject.name = gameObject.name.Replace("(Clone)", string.Empty);
    }
}
