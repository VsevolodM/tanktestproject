﻿using UnityEngine;

public static class TransformExtensions
{
    public static Vector3 GetRandomPointInside(this Transform transform)
    {
        Vector3 randomPosition = new Vector3(Random.Range(-0.5f, 0.5f), 0f, Random.Range(-0.5f, 0.5f));
        randomPosition = transform.TransformPoint(randomPosition) - transform.transform.position.y * Vector3.up;
        return randomPosition;
    }
}
