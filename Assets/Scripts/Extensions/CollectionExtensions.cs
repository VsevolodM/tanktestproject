﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CollectionExtensions
{
    public static T GetRandom<T>(this IEnumerable<T> enumerable)
    {
        int max = enumerable.Count();

        if (max == 0)
        {
            return default(T);
        }

        return enumerable.ElementAt(Random.Range(0, max));
    }
}
