﻿using System;
using UnityEngine;


public class CameraMouseRotator : MonoBehaviour
{
    public event Action<Vector3> OnCameraRotatedEvent;

    [SerializeField] private Transform target;
    [SerializeField] private float scaleSensitivity;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Camera camera;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        float xRotation = Input.GetAxis("Mouse X");
        camera.transform.RotateAround(target.transform.position, Vector3.up, xRotation);

        OnCameraRotatedEvent?.Invoke(camera.transform.eulerAngles);

        float mouseWheelInput = Input.GetAxis("Mouse ScrollWheel");

        AddOffset(-mouseWheelInput * scaleSensitivity);

        Vector3 moveOffset = target.transform.position - transform.position - offset;
        transform.position += moveOffset;

        camera.transform.LookAt(target);
    }

    private void AddOffset(float zOfffset)
    {
        offset += Vector3.forward * zOfffset;
    }
}
