﻿using System;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    public event Action<int> OnWeaponChangedEvent;
    public event Action OnShootPressedEvent;

    [SerializeField] private float forwardMovingSpeed;
    [SerializeField] private float backwardMovingSpeed;
    [SerializeField] private float rotatingSpeed;
    [SerializeField] private Transform head;
    [SerializeField] private ProjectileWeaponView rocketLauncher;
    [SerializeField] private KeyCode previousWeaponKey;
    [SerializeField] private KeyCode nextWeaponKey;
    [SerializeField] private KeyCode shootKey;

    public int WeaponIndex { get; private set; }
    private int weaponAmount;

    public void SetWeaponAmount(int amount)
    {
        weaponAmount = amount;
    }

    private void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");

        float speed = (verticalInput > 0) ? forwardMovingSpeed : backwardMovingSpeed;
        float offset = speed * verticalInput;

        if (offset != 0f)
        {
            transform.position += transform.forward * offset;
        }

        transform.eulerAngles = transform.eulerAngles + Vector3.up * horizontalInput * verticalInput;

        CheckWeaponKeys();
    }

    private void CheckWeaponKeys()
    {
        if (Input.GetKeyDown(previousWeaponKey))
        {
            WeaponIndex = (int)Mathf.Repeat(WeaponIndex - 1, weaponAmount);
            OnWeaponChangedEvent?.Invoke(WeaponIndex);
        }

        if (Input.GetKeyDown(nextWeaponKey))
        {
            WeaponIndex = (int)Mathf.Repeat(WeaponIndex + 1, weaponAmount);
            OnWeaponChangedEvent?.Invoke(WeaponIndex);
        }

        if (Input.GetKeyDown(shootKey))
        {
            OnShootPressedEvent?.Invoke();
        }
    }

    public void SetHeadRotation(Vector3 headEueler)
    {
        head.transform.eulerAngles = headEueler.y * Vector3.up;
    }
}
